# README #

## SMinify ##

* SMinify is a handy command line tool for minifying JavaScript, JSON and CSS code
* Version 0.0.1

## Installation ##

* You need python installed to run SMinify
* To set it up first download the source code 
* On linux copy the file to `/usr/local/bin` and remove the `.py` extension then run it as follows:
* `sminify [options] source [destination]`
* On windows(at lest for now), you can just run it as follows:
* `python sminify.py [options] source [destination]`

## Features and Usage ##

* Can minify JavaScript, JSON, and CSS code
* Works on the terminal
* To minify, for example, some JavaScript you can run it as follows:(showing how it runs on linux)
* `sminify '/home/user/Projects/cool_project/source.js' '/home/user/Projects/cooler_project/destination.js'`
* If you don't specify the destination file, then it'll create one for you in the same directory. 
* For example: 
* If the source file is called `source.js` then it'll create `source.min.js` in the same directory. 


## Author ##

* Schambach Milimu [(msschambach@gmail.com)](mailto:msschambach@gmail.com)
* Please feel free to get in touch and contribute to this marvelous project.
* Also working on a version for Node JS
