#!/usr/bin/env python

import sys, re, os

"""
This is a program for minifying JavaScript, CSS and JSON code

    Application Name: SMinify 
    Version: 0.0.1 
    Author: Schambach Milimu <<msschambach@gmail.com>>
"""


# Function for showing details about this program

def about(verbose = False):

    version, author, author_email, name = ('0.0.1', 'Schambach Milimu','msschambach@gmail.com', 'SMinify')

    if(verbose):

        print "{0}: v{1}".format(name, version) + '\n'

        print "Author: {0} <<{1}>>".format(author, author_email) + '\n'

    else:

        print "{0}: v{1}".format(name, version) + '\n'



# Function for displaying help

def show_help():

    about()

    print 'Usage:\n'

    print '\tsminify [options] source file [destination file]\n\n'

    print 'Options:\n'

    print '\t--help                         -h Display help message\n'

    print '\t--version                      -v Display this application version\n'

    print '\n'



# Function that does the minifying

def minify(args):
    try:

        # Open the source file

        source = open(args[1],'r')

        # Get the source file path and extension

        source_path, source_ext = os.path.splitext(source.name)


        # Make sure we're dealing with supported file types

        if not (source_ext in ('.js', '.css', '.json')):

            print 'SMinify: Invalid file type!'

            print 'Expecting either JavaScript CSS or JSON'

            source.close()

            sys.exit(0)



        # Open the destination file

        dest = '' # Variable for storing the destination file

        if len(args) >= 3:

            dest = open(args[2],'w')

        else:

            dest = open(source_path + '.min' + source_ext,'w')



        # Read the source content and minify it

        min_cont = "" # Variable for holding the minified contents

        with source as sf:

            for line in sf:

                if re.search('^(\s*)//',line):

                    line = '/*' + line.replace('//','') + '*/'

                    min_cont += line.replace('\n',' ')

                else:

                    min_cont += line.replace('\n',' ')



        # Reduce white space as much as possible

        for n in range(0, 15):

            min_cont = min_cont.replace('  ',' ')



        # Reduce any unecessary spaces

        min_cont = min_cont.replace(' : ', ':').replace(' :', ':')


        min_cont = min_cont.replace(' ; ', ';').replace('; ', ';')


        # Only for JavaScript Files

        if source_ext == '.js':

            min_cont = min_cont.replace(' = ', '=').replace(' =', '=').replace('= ', '=')


            min_cont = min_cont.replace(' == ', '==').replace(' ==', '==').replace('== ', '==')


            min_cont = min_cont.replace(' === ', '===').replace(' ===', '===').replace('=== ', '===')


            min_cont = min_cont.replace(' !== ','!==').replace(' !==','!==').replace('!== ','!==')





        # Write to the destination file the minified contents

        dest.write(min_cont)


        # Close all files

        source.close()

        dest.close()

        sys.exit(0)



    except IOError as e:

        # Handle file error

        print "SMinify: Cannot find '{0}': {1}".format(e.filename, e.strerror)

        sys.exit(0)







# Main Program


# Check if the correct parameters have been provided

if len(sys.argv) <= 1:

    about()

    print "Try 'sminify --help' or 'sminify -h' to see help information\n"

    sys.exit(0)


if(sys.argv[1] in ('--help', '-h')):

    show_help()

    sys.exit(0)

elif(sys.argv[1] in ('--version', '-v')):

    about(True)

    sys.exit(0)
else:
    minify(sys.argv)
    
    
